﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO produto)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            int id = produtoDB.Salvar(produto);
            return id;
        }
        public void Alterar(ProdutoDTO produto)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            produtoDB.Alterar(produto);
        }
        public void Remover(int idProduto)
        {
            ProdutoDatabase produtoDB = new ProdutoDatabase();
            produtoDB.Remover(idProduto);
        }
        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }

    }
}

