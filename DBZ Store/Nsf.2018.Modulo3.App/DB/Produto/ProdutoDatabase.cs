﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO produto)
        {
            string script =
            @"INSERT INTO tb_produto (id_produto , nm_produto , vl_preco)
                VALUES (@id_produto , @nm_produto , @vl_preco)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", produto.Id));
            parms.Add(new MySqlParameter("nm_produto", produto.Nome));
            parms.Add(new MySqlParameter("vl_preco", produto.Valor));
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(ProdutoDTO produto)
        {
            string script =
            @"UPDATE tb_produto SET nm_produto = @nm_produto,
                vl_produto = @vl_produto,
                WHERE id_produto = @id_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", produto.Id));
            parms.Add(new MySqlParameter("nm_produto", produto.Nome));
            parms.Add(new MySqlParameter("vl_produto", produto.Valor));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int idproduto)
        {
            string script =
            @"DELETE FROM tb_produto WHERE id_produto = @id_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", idproduto));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<ProdutoDTO> Listar()
        {
            string script =
            @"SELECT * FROM tb_produto";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<ProdutoDTO> produtos = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO produto = new ProdutoDTO();
                produto.Id = reader.GetInt32("id_produto");
                produto.Nome = reader.GetString("nm_produto");
                produto.Valor = reader.GetDecimal("vl_produto");

                produtos.Add(produto);
            }
            reader.Close();
            return produtos;
        }
    }
}
